import { add } from './add';

describe('add helper', () => {
    describe('when adding two numbers', () => {
        test('should return the sum', () => {
            expect(add(2, 2)).toEqual(4);
        });
    });
})
